/* alphabetical function */
function wordOrder(myWord) {
    var wordArray = myWord.split("");
    wordArray.sort();
    var sortedmyWord = wordArray.join("");
    return sortedmyWord
}

let alphabeticalWord = wordOrder("webmaster");
console.log(alphabeticalWord);


/* vowels function */
function vowelLength(mySentence) {
    let vowels = 0;
    for (let n = 0; n < mySentence.length; n++) {
        if (
            mySentence[n].toLowerCase() == "a" ||
            mySentence[n].toLowerCase() == "e" ||
            mySentence[n].toLowerCase() == "i" ||
            mySentence[n].toLowerCase() == "o" ||
            mySentence[n].toLowerCase() == "u"
        ) {
            vowels++;

        } else {
            continue;
        };
    };

    return vowels;


};

let myVowelLength = vowelLength("The quick brown fox");
console.log(myVowelLength);

/* array alphabetical function */

function southeastAsiaFunction(seaCountry) {
    let myArray = seaCountry.sort();

    return myArray;
}

let myCountries = southeastAsiaFunction(["Philippines", "Indonesia", "Vietnam", "Thailand"]);
console.log(myCountries);

/* Object literals */

let person = {
    firstName: "Kobe",
    lastName: "Bryant",
    age: 42,
    gender: "male",
    nationality: "American"
};
console.log(person);

/* Constructor function */
function OPM(title, artist) {
    this.title = title
    this.artist = artist
};


